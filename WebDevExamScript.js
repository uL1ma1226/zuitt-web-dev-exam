const findFirstSequence = (String1, String2) => {
    let subsequence = ""
  
    let string1Idx = 0
    let string2Idx = 0
    
    // first loop through string1 and string2 to find the first matching character
    for (string1Idx; string1Idx < String1.length; ++string1Idx) {
      const string1Char = String1[string1Idx]
      for (let j = string2Idx; j < String2.length; ++j, ++string2Idx) {
        const string2char = String2[j]
        if (string1Char === string2char) {
          subsequence += string1Char
            ++string2Idx
          break
        }
      }
    }
    return subsequence
  }
  
  // remove all characters that are not in the other string
  const removeDistinctChars = (String1, String2) => String1.split("").filter(char => String2.includes(char)).join("")
  // remove duplicates from array
  const removeDuplicates = (arr) => Array.from(new Set(arr))
  
  // find all subsequences of String1 and String2
  const findAllSubsequences = (String1, String2) => {
    const string1NoDistinct = removeDistinctChars(String1, String2)
    const string2NoDistinct = removeDistinctChars(String2, String1)
  
    let i = 0
  
    const sequences = []
  
    // loop through string1 and string2 to find all subsequences
    while (i < string1NoDistinct.length) {
      const sequence = findFirstSequence(string1NoDistinct.slice(i), string2NoDistinct)
      i += sequence.length
      sequences.push(sequence)
    }
    return sequences
  }
  
  // find longest subsequence of two strings
  const findLongestSubsequence = (String1, String2) => {
    const a = findAllSubsequences(String1, String2)
    const b = findAllSubsequences(String2, String1)
  
    console.log('Subsequences: ', [...a, ...b])
    
    // remove duplicates from array and sort by length
    return removeDuplicates([...a, ...b].sort((String1, String2) => String2.length - String1.length).filter((el, idx, arr) => el.length ===
      arr[0].length))
  }
  
  const test = (String1, String2) => console.log("Longest Subsequence: " + findLongestSubsequence(String1, String2))
  
  test("DSAFGAGW", "FASDFGWEG")
  test("ASDF", "AGSD")
  test("ASDJFGHASKLDF", "SADIHJFIW")
  test("aaaa", "aa")

